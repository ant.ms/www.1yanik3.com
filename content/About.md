---
title: "About"
date: 2019-02-16T00:00:00+01:00
draft: false
image: img/background.png
toc: false
---

Welcome to www.1Yanik3.com this is my website where I show some of my things off. Its pretty empty here, I know but I will post stuff in the future. 

<!--more--> 
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

**About me:**
My name is Yanik Ammann, also known as 1Yanik3 and host13. I'm a nerd and hobby developer. In the Summer of 2019 I start apprenticeship at Siemens. I've got some experience with coding websites, applications and simple games. I’m interested in Linux, programming and science mainly space. 

**You can find me at:**
<br>[<i class="fas fa-envelope"></i> info@1yanik3.com](mailto:info@1yanik3.com)
<br>[<i class="fab fa-reddit"></i> u/1Yanik3](https://www.reddit.com/user/1Yanik3)
<br>[<i class="fab fa-gitlab"></i> Yanik Ammann](https://gitlab.com/1Yanik3)
<br>[<i class="fab fa-youtube"></i> 1Yanik3](https://www.youtube.com/channel/UCaHyfi179fosQT2GNlHY7mg?&ab_channel=1Yanik3)



All of my projects are free to use and modify, so do whatever you want with them. If you have any questions or feedback to anything on my site, send me an email to [info@1yanik3.com](mailto:info@1yanik3.com). 


**Sites hosted by me:**

| **Site Name**      | **URL**                                                        | Type         | Engine    | Status         |
| ------------------ | ------------------------------------------------------------   | ------------ | --------- | -------------- |
| 1Yanik3            | [www.1yanik3.com](http://www.1yanik3.com/)                     | Blog         | Hugo      | **Online**     |
| Alex Heimservice   | [www.alex-heimservice.ch](http://www.alex-heimservice.ch/)     | Business     | Wordpress | **Online**     |
| La’a Kea           | [www.laakea.ch](http://www.laakea.ch)                          | Business     | Wordpress | **Online**     |
| Kai Steinke        | [www.kaisteinke.ch](http://www.kaisteinke.ch)                  | Empty        | HTML      | **Online**     |
| Kai Steinke        | [www.kaisteinke.com](http://www.kaisteinke.com)                | Portfolio    | HTML      | **Online**     |
| Natur Gestecke     | [www.natur-gestecke.ch](http://www.natur-gestecke.ch)          | Product Page | Wordpress | **Online**     |
| Venus Goldschmiede | [www.venus-goldschmiede.ch](http://www.venus-goldschmiede.ch/) | Business     | Wordpress | **Incomplete** |
| Duck Company       | [www.duckcompany.info](http://www.duckcompany.info/)           | Empty        | HTML      | **Unused**     |

**Subsites:**

| Site Name (+ URL)                          | Type                 | Engine       | Status     |
| ------------------------------------------ | -------------------- | ------------ | ---------- |
| [Yanik Ammann](http://me.1yanik3.com/)     | Portfolio (Personal) | HTTP         | **Online** |
| [Project Archive](http://apps.1Yanik3.com) | Project Archive      | Apache Share | **Online** |

**Other Sites:**

| **Site Name**    | **URL**                                                      | Type     | Engine    | Status         |
|----------------- | ------------------------------------------------------------ | -------- | --------- | -------------- |
| Visual-Gallery   | [www.visual-gallery.ch](https://www.visual-gallery.ch/de/)   | Business | Wordpress | **Online**     |
| Paradise To Rent | [www.paradise.to-rent.com](http://www.paradise-to-rent.com/) | Business | Wordpress | **Incomplete** |
