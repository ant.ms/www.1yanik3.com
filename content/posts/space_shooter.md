---
title: "Game: Space Shooter"
date: 2018-03-01T20:20:56+01:00
categories: [development, game]
tags: [game,scratch,1yanik3]
draft: false
tags:
- games
- Developement
---

Space Shooter is a game I've coded, in two hours, while at Ergon in Switzerland.
<!--more-->

Its free to play and doesn't need to be installed, just click this link to open and play this game online: [app.1yanik3.com](http://app.1yanik3.com/APP/Space%20Shooter/).
