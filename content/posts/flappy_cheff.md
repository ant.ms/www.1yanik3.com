---
title: "Game: Flappy Cheff"
date: 2018-02-06T19:39:11+01:00
categories: [development, game]
tags: [game,google play,1yanik3]
draft: false
tags:
- games
- Developement
---


Flappy Cheff is a simple game where you need to jump between walls without hitting them. How many points do you manage to achieve?

<!--more-->


Download here: [Google Play ](http://play.google.com/store/apps/details?id=com.host13.flappyCheff)

Or play online without downloading here: [app.1yanik3.com](http://app.1yanik3.com/APP/FC/v0.2/)
