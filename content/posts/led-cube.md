---
title: "3D-Modell: LED-Cube"
date: 2018-12-24T13:22:39+01:00
draft: false
tags:
- LED
- Thingiverse
- STL
---

This is an LED-Cube that I created as a Christmas gift for my family.

<!--more-->

![](/img/LED-Cube1.jpg) 

It consists of an cheap Circuit-Board responsible for charging, a battery, a switch and an LED. So its a pretty primitive construction. 

I modeled this Cube in OnShape in order to get some training in semi-professional modeling. The files are free to use, modify and publish so do whatever you want with them.

Download:
<br> **stl:** [ Part 1](/files/LED-Cube - Part 1.stl), [Part 2](/files/LED-Cube - Part 2.stl)
<br> **online:** [Thingiverse](https://www.thingiverse.com/thing:3330761), [OnShape](https://cad.onshape.com/documents/aee41ad727ff9fbcbd3ab7c0/w/7ed4e287fb53366b5ed5a2fa/e/b2e8e82c164ab686fce6cdc0)

